package com.example.springwebapi.services.character;

import com.example.springwebapi.models.Character;
import com.example.springwebapi.models.Movie;
import com.example.springwebapi.services.CrudService;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;

import java.util.Set;


public interface CharacterService extends CrudService<Character,Integer> {

    public Set<Character> findAllCharactersByFranchise(Integer id);

}
