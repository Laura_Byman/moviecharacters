package com.example.springwebapi.services.movie;

import com.example.springwebapi.models.Character;
import com.example.springwebapi.models.Movie;
import com.example.springwebapi.services.CrudService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Set;


public interface MovieService  extends CrudService<Movie, Integer> {
    public Set<Character> getAllCharactersInMovie(Integer id);
    public void updateMoviesCharactersById(int movieId, ArrayList<Integer> characterIDs);
}
