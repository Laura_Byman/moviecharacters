package com.example.springwebapi.mappers;

import com.example.springwebapi.models.Character;
import com.example.springwebapi.models.Franchise;
import com.example.springwebapi.models.Movie;
import com.example.springwebapi.models.dtos.character.CharacterDTO;
import com.example.springwebapi.services.franchise.FranchiseService;
import com.example.springwebapi.services.movie.MovieService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class CharacterMapper {

 
    @Autowired
    protected MovieService movieService;

    @Mapping(target = "movies", source = "movies", qualifiedByName = "moviesToIds")
    public abstract CharacterDTO characterToCharacterDto(Character character);

    public abstract Collection<CharacterDTO> characterToCharacterDto(Collection<Character> characters);

    @Mapping(target = "movies", source = "movies", qualifiedByName = "movieIdsToMovies")
    public abstract Character characterDtoToCharacter(CharacterDTO dto);

    @Named("movieIdsToMovies")
    Set<Movie> mapIdsToMovies(Set<Integer> id) {
        return id.stream()
                .map( i -> movieService.findById(i))
                .collect(Collectors.toSet());
    }

    @Named("moviesToIds")
    Set<Integer> mapMoviesToIds(Set<Movie> source) {
        if(source == null)
            return null;
        return source.stream()
                .map(s -> s.getId()).collect(Collectors.toSet());
    }
}

