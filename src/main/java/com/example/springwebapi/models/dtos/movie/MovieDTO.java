package com.example.springwebapi.models.dtos.movie;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.Set;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class MovieDTO {
    private int id;
    private String title;
    private String genre;
    private String release;
    private String director;
    private String picture;
    private String trailer;
    private Set<Integer> characters;
    private int franchise;
}
