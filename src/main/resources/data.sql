INSERT INTO franchise (name, description) VALUES ('Tarantino movies', 'Films directed by Quentin Tarantino');
INSERT INTO franchise (name, description) VALUES ('Kubrick Movies', 'Films directed by Stanley Kubrick');
INSERT INTO franchise (name, description) VALUES ('Lord of the Rings', 'Epic fantasy adventure saga taking place in the world of Middle Earth');
INSERT INTO franchise (name, description) VALUES ('Matrix', 'The series features a cyberpunk story of the technological fall of humanity, in which the creation of artificial intelligence led the way to a race of self-aware machines that imprisoned mankind in a virtual reality system—the Matrix—to be farmed as a power source. ');

INSERT INTO character (name, gender, picture) VALUES ('Oscar','Male', 'https://www.interviewmagazine.com/wp-content/uploads/2010/09/img-nathan1_125928454971.jpg');
INSERT INTO character (name, gender, picture) VALUES ('Mia Wallace', 'Female', 'https://dazedimg-dazedgroup.netdna-ssl.com/900/azure/dazed-prod/1140/5/1145937.jpg');
INSERT INTO character (name, alias, gender, picture) VALUES ('Frodo Baggings', 'The Ringbearer', 'Male', 'https://upload.wikimedia.org/wikipedia/en/4/4e/Elijah_Wood_as_Frodo_Baggins.png');
INSERT INTO character (name, alias, gender, picture) VALUES ('Gandalf the Grey', 'Gandalf the White', 'Male', 'https://static.wikia.nocookie.net/lotr/images/e/e7/Gandalf_the_Grey.jpg/revision/latest/scale-to-width-down/350?cb=20121110131754');
INSERT INTO character (name, alias, gender, picture) VALUES ('Thomas A. Anderson', 'Neo', 'Male', 'https://upload.wikimedia.org/wikipedia/en/c/c6/NeoTheMatrix.jpg');
INSERT INTO character (name, gender, picture) VALUES ('Trinity',  'Female', 'https://upload.wikimedia.org/wikipedia/en/7/7a/MatrixTrinity.jpg');
INSERT INTO character (name,  gender, picture) VALUES ('Django Freeman',  'Male', 'https://m.media-amazon.com/images/M/MV5BODUyNzEwNDkzNl5BMl5BanBnXkFtZTcwMzcwMDYzOA@@._V1_.jpg');

INSERT INTO movie (director, genre, title, release, picture,  trailer, franchise_id) VALUES ('Quentin Tarantino', 'Drama/Action', 'Pulp Fiction', '1994', 'https://i-viaplay-com.akamaized.net/viaplay-prod/771/672/1473257890-66ec43721fe0fd0073af100473a09da74924816c.jpg?width=400&height=600',  'https://www.imdb.com/title/tt0110912/',2);
INSERT INTO movie (director, genre, title, release, picture,  trailer, franchise_id) VALUES ('Stanley Kubrick', 'Crime', 'A Clockwork Orange', '1971','https://upload.wikimedia.org/wikipedia/en/7/73/A_Clockwork_Orange_%281971%29.png', 'https://www.imdb.com/title/tt0066921/',1);
INSERT INTO movie (director, genre, title, release, picture,  trailer, franchise_id) VALUES ('Peter Jackson', 'Fantasy/Adventure', 'Lord of the Rings Fellowship of the Rings', '2001','https://upload.wikimedia.org/wikipedia/en/8/8a/The_Lord_of_the_Rings_The_Fellowship_of_the_Ring_%282001%29.jpg', 'https://www.youtube.com/watch?v=_e8QGuG50ro', 3);
INSERT INTO movie (director, genre, title, release, picture,  trailer, franchise_id) VALUES ('Peter Jackson', 'Fantasy/Adventure', 'Lord of the Rings Fellowship of the Rings', '2001','https://upload.wikimedia.org/wikipedia/fi/thumb/2/2a/LOTRTTTmovie.jpg/250px-LOTRTTTmovie.jpg', 'https://www.youtube.com/watch?v=WGAuIniKeEU', 3);
INSERT INTO movie (director, genre, title, release, picture,  trailer, franchise_id) VALUES ('Lana WachowskiLilly, Wachowski', 'Scifi', 'Matrix', '1999','https://prod.cdn.bbaws.net/TDC_Blockbuster_-_Production/607/724/WB-0044_po-reg-medium_orig.jpg', 'https://www.youtube.com/watch?v=m8e-FF8MsqU', 4);
INSERT INTO movie (director, genre, title, release, picture,  trailer, franchise_id) VALUES ('Lana WachowskiLilly, Wachowski', 'Scifi', 'Matrix Reloaded', '2003','https://upload.wikimedia.org/wikipedia/fi/thumb/9/97/Matrix_reloaded_ver14.jpg/375px-Matrix_reloaded_ver14.jpg', 'https://www.youtube.com/watch?v=zmYE3tg26Qc', 4);
INSERT INTO movie (director, genre, title, release, picture,  franchise_id) VALUES ('Quentin Tarantino', 'Drama/Action', 'Django Unchained', '2012','https://upload.wikimedia.org/wikipedia/fi/thumb/3/34/Django-unchained.jpg/375px-Django-unchained.jpg', 1);

INSERT INTO movie_character(movie_id, character_id) VALUES ('1', '1');
INSERT INTO movie_character(movie_id, character_id) VALUES ('2', '2');
INSERT INTO movie_character(movie_id, character_id) VALUES ('3', '3');
INSERT INTO movie_character(movie_id, character_id) VALUES ('4', '3');
INSERT INTO movie_character(movie_id, character_id) VALUES ('4', '4');
INSERT INTO movie_character(movie_id, character_id) VALUES ('5', '5');
INSERT INTO movie_character(movie_id, character_id) VALUES ('6', '5');
INSERT INTO movie_character(movie_id, character_id) VALUES ('5', '6');
INSERT INTO movie_character(movie_id, character_id) VALUES ('6', '6');
INSERT INTO movie_character(movie_id, character_id) VALUES ('7', '7');